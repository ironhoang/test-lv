export default {
    "columns": {
        "id": {
            "name": "Id",
            "type": "text",
            "editable": false,
            "queryName": "id",
            "apiName": "id"
        },
        "name": {
            "name": "Name",
            "type": "text",
            "editable": true,
            "queryName": "name",
            "apiName": "name"
        },
        "content": {
            "name": "Content",
            "type": "textarea",
            "editable": true,
            "queryName": "content",
            "apiName": "content"
        },
        "created_at": {
            "name": "Created At",
            "type": "text",
            "editable": false,
            "queryName": "created_at",
            "apiName": "createdAt"
        },
        "updated_at": {
            "name": "Updated At",
            "type": "text",
            "editable": false,
            "queryName": "updated_at",
            "apiName": "updatedAt"
        }
    },
    "list": [
        "name"
    ],
    "show": [
        "id",
        "name",
        "content",
        "created_at",
        "updated_at"
    ],
    "edit": [
        "name",
        "content"
    ]
};

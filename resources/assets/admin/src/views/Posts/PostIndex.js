import React from "react";

import PostRepository from "../../repositories/PostRepository";
import columns from './_columns'
import info from './_info'
import {withRouter} from 'react-router-dom'
import Index from "../CRUDBase/Index";

class PostIndex extends Index {

  setPageInfo() {
    this.title = info.title;
    this.path = info.path;
  }

  setRepository() {
    this.repository = new PostRepository();
  }

  setColumnInfo() {
    this.columns = columns;
  }

}

export default withRouter(PostIndex);

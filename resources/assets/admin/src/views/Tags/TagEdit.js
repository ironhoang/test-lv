import React from "react";

import TagRepository from "../../repositories/TagRepository";
import columns from './_columns'
import info from "./_info";
import {withRouter} from 'react-router-dom'
import Edit from "../CRUDBase/Edit";

class TagEdit extends Edit {

  setPageInfo() {
    this.title = info.title;
    this.path = info.path;
  }

  setRepository() {
    this.repository = new TagRepository();
  }

  setColumnInfo() {
    this.columns = columns;
  }
}

export default withRouter(TagEdit);

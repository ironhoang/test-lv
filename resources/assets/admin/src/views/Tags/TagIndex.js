import React from "react";

import TagRepository from "../../repositories/TagRepository";
import columns from './_columns'
import info from './_info'
import {withRouter} from 'react-router-dom'
import Index from "../CRUDBase/Index";

class TagIndex extends Index {

  setPageInfo() {
    this.title = info.title;
    this.path = info.path;
  }

  setRepository() {
    this.repository = new TagRepository();
  }

  setColumnInfo() {
    this.columns = columns;
  }

}

export default withRouter(TagIndex);

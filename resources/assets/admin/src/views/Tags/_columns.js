export default {
    "columns": {
        "id": {
            "name": "Id",
            "type": "text",
            "editable": false,
            "queryName": "id",
            "apiName": "id"
        },
        "name": {
            "name": "Name",
            "type": "text",
            "editable": true,
            "queryName": "name",
            "apiName": "name"
        },
        "slug": {
            "name": "Slug",
            "type": "text",
            "editable": true,
            "queryName": "slug",
            "apiName": "slug"
        },
        "created_at": {
            "name": "Created At",
            "type": "text",
            "editable": false,
            "queryName": "created_at",
            "apiName": "createdAt"
        },
        "updated_at": {
            "name": "Updated At",
            "type": "text",
            "editable": false,
            "queryName": "updated_at",
            "apiName": "updatedAt"
        }
    },
    "list": [
        "name",
        "slug"
    ],
    "show": [
        "id",
        "name",
        "slug",
        "created_at",
        "updated_at"
    ],
    "edit": [
        "name",
        "slug"
    ]
};

export default {
    "columns": {
        "id": {
            "name": "Id",
            "type": "text",
            "editable": false,
            "queryName": "id",
            "apiName": "id"
        },
        "admin_user_id": {
            "name": "Admin User Id",
            "type": "text",
            "editable": true,
            "queryName": "admin_user",
            "apiName": "adminUser"
        },
        "type": {
            "name": "Type",
            "type": "select",
            "editable": true,
            "queryName": "type",
            "apiName": "type"
        },
        "data": {
            "name": "Data",
            "type": "textarea",
            "editable": true,
            "queryName": "data",
            "apiName": "data"
        },
        "notified_at": {
            "name": "Notified At",
            "type": "text",
            "editable": true,
            "queryName": "notified_at",
            "apiName": "notifiedAt"
        },
        "read_at": {
            "name": "Read At",
            "type": "text",
            "editable": true,
            "queryName": "read_at",
            "apiName": "readAt"
        },
        "adminUser": {
            "name": "Admin User",
            "type": "select_single",
            "editable": true,
            "queryName": "admin_user",
            "apiName": "adminUser",
            "options": [],
            "optionNames": []
        }
    },
    "list": [
        "admin_user_id",
        "type",
        "notified_at",
        "read_at",
        "adminUser"
    ],
    "show": [
        "id",
        "admin_user_id",
        "type",
        "data",
        "notified_at",
        "read_at"
    ],
    "edit": [
        "admin_user_id",
        "type",
        "data",
        "notified_at",
        "read_at",
        "adminUser"
    ]
};

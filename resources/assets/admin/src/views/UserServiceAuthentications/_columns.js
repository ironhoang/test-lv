export default {
    "columns": {
        "id": {
            "name": "Id",
            "type": "text",
            "editable": false,
            "queryName": "id",
            "apiName": "id"
        },
        "name": {
            "name": "Name",
            "type": "text",
            "editable": true,
            "queryName": "name",
            "apiName": "name"
        },
        "email": {
            "name": "Email",
            "type": "email",
            "editable": true,
            "queryName": "email",
            "apiName": "email"
        },
        "service": {
            "name": "Service",
            "type": "text",
            "editable": true,
            "queryName": "service",
            "apiName": "service"
        },
        "service_id": {
            "name": "Service Id",
            "type": "text",
            "editable": true,
            "queryName": "service_id",
            "apiName": "serviceId"
        },
        "image_url": {
            "name": "Image Url",
            "type": "text",
            "editable": true,
            "queryName": "image_url",
            "apiName": "imageUrl"
        }
    },
    "list": [
        "name",
        "email",
        "service",
        "service_id",
        "image_url"
    ],
    "show": [
        "id",
        "name",
        "email",
        "service",
        "service_id",
        "image_url"
    ],
    "edit": [
        "name",
        "email",
        "service",
        "service_id",
        "image_url"
    ]
};

export default {
    "columns": {
        "id": {
            "name": "Id",
            "type": "text",
            "editable": false,
            "queryName": "id",
            "apiName": "id"
        },
        "admin_user_id": {
            "name": "Admin User Id",
            "type": "text",
            "editable": true,
            "queryName": "admin_user",
            "apiName": "adminUser"
        },
        "role": {
            "name": "Role",
            "type": "text",
            "editable": true,
            "queryName": "role",
            "apiName": "role"
        },
        "created_at": {
            "name": "Created At",
            "type": "text",
            "editable": false,
            "queryName": "created_at",
            "apiName": "createdAt"
        },
        "updated_at": {
            "name": "Updated At",
            "type": "text",
            "editable": false,
            "queryName": "updated_at",
            "apiName": "updatedAt"
        },
        "adminUser": {
            "name": "Admin User",
            "type": "select_single",
            "editable": true,
            "queryName": "admin_user",
            "apiName": "adminUser",
            "options": [],
            "optionNames": []
        }
    },
    "list": [
        "admin_user_id",
        "role",
        "adminUser"
    ],
    "show": [
        "id",
        "admin_user_id",
        "role",
        "created_at",
        "updated_at"
    ],
    "edit": [
        "admin_user_id",
        "role",
        "adminUser"
    ]
};

import React from "react";

import CategoryRepository from "../../repositories/CategoryRepository";
import columns from './_columns'
import info from "./_info";
import {withRouter} from 'react-router-dom'
import Show from "../CRUDBase/Show";

class CategoryShow extends Show {

  setPageInfo() {
    this.title = info.title;
    this.path = info.path;
  }

  setRepository() {
    this.repository = new CategoryRepository();
  }

  setColumnInfo() {
    this.columns = columns;
  }

}

export default withRouter(CategoryShow);

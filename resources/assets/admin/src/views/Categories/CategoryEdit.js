import React from "react";

import CategoryRepository from "../../repositories/CategoryRepository";
import columns from './_columns'
import info from "./_info";
import {withRouter} from 'react-router-dom'
import Edit from "../CRUDBase/Edit";

class CategoryEdit extends Edit {

  setPageInfo() {
    this.title = info.title;
    this.path = info.path;
  }

  setRepository() {
    this.repository = new CategoryRepository();
  }

  setColumnInfo() {
    this.columns = columns;
  }
}

export default withRouter(CategoryEdit);

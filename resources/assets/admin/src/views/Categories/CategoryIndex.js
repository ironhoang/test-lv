import React from "react";

import CategoryRepository from "../../repositories/CategoryRepository";
import columns from './_columns'
import info from './_info'
import {withRouter} from 'react-router-dom'
import Index from "../CRUDBase/Index";

class CategoryIndex extends Index {

  setPageInfo() {
    this.title = info.title;
    this.path = info.path;
  }

  setRepository() {
    this.repository = new CategoryRepository();
  }

  setColumnInfo() {
    this.columns = columns;
  }

}

export default withRouter(CategoryIndex);

export default {
    "columns": {
        "id": {
            "name": "Id",
            "type": "text",
            "editable": false,
            "queryName": "id",
            "apiName": "id"
        },
        "url": {
            "name": "Url",
            "type": "text",
            "editable": true,
            "queryName": "url",
            "apiName": "url"
        },
        "title": {
            "name": "Title",
            "type": "text",
            "editable": true,
            "queryName": "title",
            "apiName": "title"
        },
        "entity_type": {
            "name": "Entity Type",
            "type": "select",
            "editable": true,
            "queryName": "entity_type",
            "apiName": "entityType"
        },
        "entity_id": {
            "name": "Entity Id",
            "type": "text",
            "editable": true,
            "queryName": "entity_id",
            "apiName": "entityId"
        },
        "storage_type": {
            "name": "Storage Type",
            "type": "select",
            "editable": true,
            "queryName": "storage_type",
            "apiName": "storageType"
        },
        "file_category_type": {
            "name": "File Category Type",
            "type": "select",
            "editable": true,
            "queryName": "file_category_type",
            "apiName": "fileCategoryType"
        },
        "file_type": {
            "name": "File Type",
            "type": "select",
            "editable": true,
            "queryName": "file_type",
            "apiName": "fileType"
        },
        "s3_key": {
            "name": "S3 Key",
            "type": "text",
            "editable": true,
            "queryName": "s3_key",
            "apiName": "s3Key"
        },
        "s3_bucket": {
            "name": "S3 Bucket",
            "type": "text",
            "editable": true,
            "queryName": "s3_bucket",
            "apiName": "s3Bucket"
        },
        "s3_region": {
            "name": "S3 Region",
            "type": "text",
            "editable": true,
            "queryName": "s3_region",
            "apiName": "s3Region"
        },
        "s3_extension": {
            "name": "S3 Extension",
            "type": "text",
            "editable": true,
            "queryName": "s3_extension",
            "apiName": "s3Extension"
        },
        "media_type": {
            "name": "Media Type",
            "type": "select",
            "editable": true,
            "queryName": "media_type",
            "apiName": "mediaType"
        },
        "format": {
            "name": "Format",
            "type": "text",
            "editable": true,
            "queryName": "format",
            "apiName": "format"
        },
        "original_file_name": {
            "name": "Original File Name",
            "type": "text",
            "editable": true,
            "queryName": "original_file_name",
            "apiName": "originalFileName"
        },
        "file_size": {
            "name": "File Size",
            "type": "text",
            "editable": true,
            "queryName": "file_size",
            "apiName": "fileSize"
        },
        "width": {
            "name": "Width",
            "type": "text",
            "editable": true,
            "queryName": "width",
            "apiName": "width"
        },
        "height": {
            "name": "Height",
            "type": "text",
            "editable": true,
            "queryName": "height",
            "apiName": "height"
        },
        "thumbnails": {
            "name": "Thumbnails",
            "type": "textarea",
            "editable": true,
            "queryName": "thumbnails",
            "apiName": "thumbnails"
        },
        "is_enabled": {
            "name": "Is Enabled",
            "type": "boolean",
            "editable": true,
            "queryName": "is_enabled",
            "apiName": "isEnabled"
        },
        "created_at": {
            "name": "Created At",
            "type": "text",
            "editable": false,
            "queryName": "created_at",
            "apiName": "createdAt"
        },
        "updated_at": {
            "name": "Updated At",
            "type": "text",
            "editable": false,
            "queryName": "updated_at",
            "apiName": "updatedAt"
        }
    },
    "list": [
        "url",
        "title",
        "entity_type",
        "entity_id",
        "storage_type",
        "file_category_type",
        "file_type",
        "s3_key",
        "s3_bucket",
        "s3_region",
        "s3_extension",
        "media_type",
        "format",
        "original_file_name",
        "file_size",
        "width",
        "height",
        "is_enabled"
    ],
    "show": [
        "id",
        "url",
        "title",
        "entity_type",
        "entity_id",
        "storage_type",
        "file_category_type",
        "file_type",
        "s3_key",
        "s3_bucket",
        "s3_region",
        "s3_extension",
        "media_type",
        "format",
        "original_file_name",
        "file_size",
        "width",
        "height",
        "thumbnails",
        "is_enabled",
        "created_at",
        "updated_at"
    ],
    "edit": [
        "url",
        "title",
        "entity_type",
        "entity_id",
        "storage_type",
        "file_category_type",
        "file_type",
        "s3_key",
        "s3_bucket",
        "s3_region",
        "s3_extension",
        "media_type",
        "format",
        "original_file_name",
        "file_size",
        "width",
        "height",
        "thumbnails",
        "is_enabled"
    ]
};

import BaseRepository from "./BaseRepository";

class TagRepository extends BaseRepository {
  constructor(){
    super();
    this.PATH = "/tags";
  }
}

export default TagRepository;

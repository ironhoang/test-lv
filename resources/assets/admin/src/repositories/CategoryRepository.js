import BaseRepository from "./BaseRepository";

class CategoryRepository extends BaseRepository {
  constructor(){
    super();
    this.PATH = "/categories";
  }
}

export default CategoryRepository;

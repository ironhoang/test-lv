import BaseRepository from "./BaseRepository";

class PostRepository extends BaseRepository {
  constructor(){
    super();
    this.PATH = "/posts";
  }
}

export default PostRepository;

export default {
    "items": [
        {
            "name": "Desktop",
            "url": "\/desktop",
            "icon": "fa fa-desktop"
        },
        {
            "divider": true
        },
        {
            "title": true,
            "name": "CRUD"
        },
        {
            "name": "Admin Users",
            "url": "\/admin-users",
            "icon": "fa fa-user-secret"
        },
        {
            "name": "Users",
            "url": "\/users",
            "icon": "fa fa-users"
        },
        {
            "name": "Files",
            "url": "\/files",
            "icon": "fa fa-file"
        },
        {
            "name": "Admin Users",
            "url": "\/admin_users",
            "icon": "fa fa-user-secret"
        },
        {
            "name": "Admin User Roles",
            "url": "\/admin_user_roles",
            "icon": "fa fa-file"
        },
        {
            "name": "Admin User Notifications",
            "url": "\/admin_user_notifications",
            "icon": "fa fa-bell"
        },
        {
            "name": "User Service Authentications",
            "url": "\/user_service_authentications",
            "icon": "fa fa-file"
        },
        {
            "name": "Categories",
            "url": "\/categories",
            "icon": "fa fa-file"
        },
        {
            "name": "Posts",
            "url": "\/posts",
            "icon": "fa fa-file"
        },
        {
            "name": "Tags",
            "url": "\/tags",
            "icon": "fa fa-tags"
        }
    ]
};

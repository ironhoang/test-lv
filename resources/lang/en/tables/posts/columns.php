<?PHP

return [
    'id' => [
        'name' => 'Id',
    ],
    'name' => [
        'name' => 'Name',
    ],
    'content' => [
        'name' => 'Content',
    ],
    'created_at' => [
        'name' => 'Created At',
    ],
    'updated_at' => [
        'name' => 'Updated At',
    ],
];

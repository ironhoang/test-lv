<?PHP

return [
    'id' => [
        'name' => 'Id',
    ],
    'name' => [
        'name' => 'Name',
    ],
    'created_at' => [
        'name' => 'Created At',
    ],
    'updated_at' => [
        'name' => 'Updated At',
    ],
];

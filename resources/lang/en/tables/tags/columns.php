<?PHP

return [
    'id' => [
        'name' => 'Id',
    ],
    'name' => [
        'name' => 'Name',
    ],
    'slug' => [
        'name' => 'Slug',
    ],
    'created_at' => [
        'name' => 'Created At',
    ],
    'updated_at' => [
        'name' => 'Updated At',
    ],
];

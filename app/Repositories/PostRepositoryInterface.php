<?php
namespace App\Repositories;

use LaravelRocket\Foundation\Repositories\SingleKeyModelRepositoryInterface;

/**
 * @method \App\Models\Post[] getEmptyList()
 * @method \App\Models\Post[]|\Traversable|array all($order = null, $direction = null)
 * @method \App\Models\Post[]|\Traversable|array get($order, $direction, $offset, $limit, $before = 0)
 * @method \App\Models\Post create($value)
 * @method \App\Models\Post find($id)
 * @method \App\Models\Post[]|\Traversable|array allByIds($ids, $order = null, $direction = null, $reorder = false)
 * @method \App\Models\Post[]|\Traversable|array getByIds($ids, $order = null, $direction = null, $offset = null, $limit = null);
 * @method \App\Models\Post update($model, $input)
 * @method \App\Models\Post save($model);
 * @method \App\Models\Post firstByFilter($filter);
 * @method \App\Models\Post[]|\Traversable|array getByFilter($filter,$order = null, $direction = null, $offset = null, $limit = null, $before = 0);
 * @method \App\Models\Post[]|\Traversable|array allByFilter($filter,$order = null, $direction = null);
 */
interface PostRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @return \App\Models\Post
     */
    public function getBlankModel();
}

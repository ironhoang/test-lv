<?php
namespace App\Repositories\Eloquent;

use App\Models\AdminUserNotification;
use App\Repositories\AdminUserNotificationRepositoryInterface;
use LaravelRocket\Foundation\Repositories\Eloquent\SingleKeyModelRepository;

class AdminUserNotificationRepository extends SingleKeyModelRepository implements AdminUserNotificationRepositoryInterface
{
    protected $querySearchTargets = [
    ];

    public function getBlankModel()
    {
        return new AdminUserNotification();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    protected function buildQueryByFilter($query, $filter)
    {
        return parent::buildQueryByFilter($query, $filter);
    }
}

<?php
namespace App\Repositories\Eloquent;

use App\Models\UserServiceAuthentication;
use App\Repositories\UserServiceAuthenticationRepositoryInterface;
use LaravelRocket\Foundation\Repositories\Eloquent\SingleKeyModelRepository;

class UserServiceAuthenticationRepository extends SingleKeyModelRepository implements UserServiceAuthenticationRepositoryInterface
{
    protected $querySearchTargets = [
        'name',
    ];

    public function getBlankModel()
    {
        return new UserServiceAuthentication();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    protected function buildQueryByFilter($query, $filter)
    {
        return parent::buildQueryByFilter($query, $filter);
    }
}

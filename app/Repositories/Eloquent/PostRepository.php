<?php
namespace App\Repositories\Eloquent;

use App\Models\Post;
use App\Repositories\PostRepositoryInterface;
use LaravelRocket\Foundation\Repositories\Eloquent\SingleKeyModelRepository;

class PostRepository extends SingleKeyModelRepository implements PostRepositoryInterface
{
    protected $querySearchTargets = [
        'name',
        'content',
    ];

    public function getBlankModel()
    {
        return new Post();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    protected function buildQueryByFilter($query, $filter)
    {
        return parent::buildQueryByFilter($query, $filter);
    }
}

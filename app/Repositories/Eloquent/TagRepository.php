<?php
namespace App\Repositories\Eloquent;

use App\Models\Tag;
use App\Repositories\TagRepositoryInterface;
use LaravelRocket\Foundation\Repositories\Eloquent\SingleKeyModelRepository;

class TagRepository extends SingleKeyModelRepository implements TagRepositoryInterface
{
    protected $querySearchTargets = [
        'name',
    ];

    public function getBlankModel()
    {
        return new Tag();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    protected function buildQueryByFilter($query, $filter)
    {
        return parent::buildQueryByFilter($query, $filter);
    }
}

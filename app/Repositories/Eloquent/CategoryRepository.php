<?php
namespace App\Repositories\Eloquent;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use LaravelRocket\Foundation\Repositories\Eloquent\SingleKeyModelRepository;

class CategoryRepository extends SingleKeyModelRepository implements CategoryRepositoryInterface
{
    protected $querySearchTargets = [
        'name',
    ];

    public function getBlankModel()
    {
        return new Category();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    protected function buildQueryByFilter($query, $filter)
    {
        return parent::buildQueryByFilter($query, $filter);
    }
}

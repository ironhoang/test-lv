<?php
namespace App\Repositories;

use LaravelRocket\Foundation\Repositories\SingleKeyModelRepositoryInterface;

/**
 * @method \App\Models\Category[] getEmptyList()
 * @method \App\Models\Category[]|\Traversable|array all($order = null, $direction = null)
 * @method \App\Models\Category[]|\Traversable|array get($order, $direction, $offset, $limit, $before = 0)
 * @method \App\Models\Category create($value)
 * @method \App\Models\Category find($id)
 * @method \App\Models\Category[]|\Traversable|array allByIds($ids, $order = null, $direction = null, $reorder = false)
 * @method \App\Models\Category[]|\Traversable|array getByIds($ids, $order = null, $direction = null, $offset = null, $limit = null);
 * @method \App\Models\Category update($model, $input)
 * @method \App\Models\Category save($model);
 * @method \App\Models\Category firstByFilter($filter);
 * @method \App\Models\Category[]|\Traversable|array getByFilter($filter,$order = null, $direction = null, $offset = null, $limit = null, $before = 0);
 * @method \App\Models\Category[]|\Traversable|array allByFilter($filter,$order = null, $direction = null);
 */
interface CategoryRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @return \App\Models\Category
     */
    public function getBlankModel();
}

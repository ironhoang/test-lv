<?php
namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\Api\Admin\APIErrorException;
use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Admin\Post\IndexRequest;
use App\Http\Requests\Api\Admin\Post\StoreRequest;
use App\Http\Requests\Api\Admin\Post\UpdateRequest;
use App\Http\Responses\Api\Admin\Post;
use App\Http\Responses\Api\Admin\Posts;
use App\Http\Responses\Api\Admin\Status;
use App\Repositories\PostRepositoryInterface;
use App\Services\AdminUserServiceInterface;
use App\Services\FileServiceInterface;

class PostController extends Controller
{
    /** @var \App\Repositories\PostRepositoryInterface */
    protected $postRepository;

    /** @var \App\Services\AdminUserServiceInterface $adminUserServicee */
    protected $adminUserService;

    /** @var \App\Services\FileServiceInterface $fileService */
    protected $fileService;

    public function __construct(
        PostRepositoryInterface $postRepository,
        FileServiceInterface $fileService,
        AdminUserServiceInterface $adminUserService
    ) {
        $this->postRepository      = $postRepository;
        $this->adminUserService    = $adminUserService;
        $this->fileService         = $fileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $offset    = $request->offset();
        $limit     = $request->limit();
        $order     = $request->order();
        $direction = $request->direction();

        $queryWord = $request->get('query');
        $filter    = [];
        if (!empty($queryWord)) {
            $filter['query'] = $queryWord;
        }

        $count      = $this->postRepository->count();
        $posts      = $this->postRepository->getByFilter($filter, $order, $direction, $offset, $limit);

        return Posts::updateListWithModel($posts, $offset, $limit, $count)->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @throws APIErrorException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $input = $request->only([
            'name',
            'content',
        ]);

        $post = $this->postRepository->create($input);

        if (empty($post)) {
            throw new APIErrorException('unknown', 'Post Creation Failed');
        }

        return Post::updateWithModel($post)->withStatus(201)->response();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @throws APIErrorException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $post = $this->postRepository->find($id);
        if (empty($post)) {
            throw new APIErrorException('notFound', 'Post not found');
        }

        return Post::updateWithModel($post)->response();
    }

    /**
     * @param int           $id
     * @param UpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Api\Admin\APIErrorException
     */
    public function update($id, UpdateRequest $request)
    {
        $post = $this->postRepository->find($id);
        if (empty($post)) {
            throw new APIErrorException('notFound', 'Post not found');
        }

        $input = $request->only([
        'name',
        'content',
        ]);

        $post = $this->postRepository->update($post, $input);

        return Post::updateWithModel($post)->response();
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Api\Admin\APIErrorException
     */
    public function destroy($id)
    {
        $post = $this->postRepository->find($id);
        if (empty($post)) {
            throw new APIErrorException('notFound', 'Post not found');
        }

        $this->postRepository->delete($post);

        return Status::ok()->response();
    }
}

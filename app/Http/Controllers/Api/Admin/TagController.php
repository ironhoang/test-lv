<?php
namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\Api\Admin\APIErrorException;
use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Admin\Tag\IndexRequest;
use App\Http\Requests\Api\Admin\Tag\StoreRequest;
use App\Http\Requests\Api\Admin\Tag\UpdateRequest;
use App\Http\Responses\Api\Admin\Status;
use App\Http\Responses\Api\Admin\Tag;
use App\Http\Responses\Api\Admin\Tags;
use App\Repositories\TagRepositoryInterface;
use App\Services\AdminUserServiceInterface;
use App\Services\FileServiceInterface;

class TagController extends Controller
{
    /** @var \App\Repositories\TagRepositoryInterface */
    protected $tagRepository;

    /** @var \App\Services\AdminUserServiceInterface $adminUserServicee */
    protected $adminUserService;

    /** @var \App\Services\FileServiceInterface $fileService */
    protected $fileService;

    public function __construct(
        TagRepositoryInterface $tagRepository,
        FileServiceInterface $fileService,
        AdminUserServiceInterface $adminUserService
    ) {
        $this->tagRepository       = $tagRepository;
        $this->adminUserService    = $adminUserService;
        $this->fileService         = $fileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $offset    = $request->offset();
        $limit     = $request->limit();
        $order     = $request->order();
        $direction = $request->direction();

        $queryWord = $request->get('query');
        $filter    = [];
        if (!empty($queryWord)) {
            $filter['query'] = $queryWord;
        }

        $count      = $this->tagRepository->count();
        $tags       = $this->tagRepository->getByFilter($filter, $order, $direction, $offset, $limit);

        return Tags::updateListWithModel($tags, $offset, $limit, $count)->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @throws APIErrorException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $input = $request->only([
            'name',
            'slug',
        ]);

        $tag = $this->tagRepository->create($input);

        if (empty($tag)) {
            throw new APIErrorException('unknown', 'Tag Creation Failed');
        }

        return Tag::updateWithModel($tag)->withStatus(201)->response();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @throws APIErrorException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $tag = $this->tagRepository->find($id);
        if (empty($tag)) {
            throw new APIErrorException('notFound', 'Tag not found');
        }

        return Tag::updateWithModel($tag)->response();
    }

    /**
     * @param int           $id
     * @param UpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Api\Admin\APIErrorException
     */
    public function update($id, UpdateRequest $request)
    {
        $tag = $this->tagRepository->find($id);
        if (empty($tag)) {
            throw new APIErrorException('notFound', 'Tag not found');
        }

        $input = $request->only([
        'name',
        'slug',
        ]);

        $tag = $this->tagRepository->update($tag, $input);

        return Tag::updateWithModel($tag)->response();
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Api\Admin\APIErrorException
     */
    public function destroy($id)
    {
        $tag = $this->tagRepository->find($id);
        if (empty($tag)) {
            throw new APIErrorException('notFound', 'Tag not found');
        }

        $this->tagRepository->delete($tag);

        return Status::ok()->response();
    }
}

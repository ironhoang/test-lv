<?php
namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\Api\Admin\APIErrorException;
use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Admin\Category\IndexRequest;
use App\Http\Requests\Api\Admin\Category\StoreRequest;
use App\Http\Requests\Api\Admin\Category\UpdateRequest;
use App\Http\Responses\Api\Admin\Categories;
use App\Http\Responses\Api\Admin\Category;
use App\Http\Responses\Api\Admin\Status;
use App\Repositories\CategoryRepositoryInterface;
use App\Services\AdminUserServiceInterface;
use App\Services\FileServiceInterface;

class CategoryController extends Controller
{
    /** @var \App\Repositories\CategoryRepositoryInterface */
    protected $categoryRepository;

    /** @var \App\Services\AdminUserServiceInterface $adminUserServicee */
    protected $adminUserService;

    /** @var \App\Services\FileServiceInterface $fileService */
    protected $fileService;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        FileServiceInterface $fileService,
        AdminUserServiceInterface $adminUserService
    ) {
        $this->categoryRepository  = $categoryRepository;
        $this->adminUserService    = $adminUserService;
        $this->fileService         = $fileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $offset    = $request->offset();
        $limit     = $request->limit();
        $order     = $request->order();
        $direction = $request->direction();

        $queryWord = $request->get('query');
        $filter    = [];
        if (!empty($queryWord)) {
            $filter['query'] = $queryWord;
        }

        $count      = $this->categoryRepository->count();
        $categories = $this->categoryRepository->getByFilter($filter, $order, $direction, $offset, $limit);

        return Categories::updateListWithModel($categories, $offset, $limit, $count)->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @throws APIErrorException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $input = $request->only([
            'name',
        ]);

        $category = $this->categoryRepository->create($input);

        if (empty($category)) {
            throw new APIErrorException('unknown', 'Category Creation Failed');
        }

        return Category::updateWithModel($category)->withStatus(201)->response();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @throws APIErrorException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = $this->categoryRepository->find($id);
        if (empty($category)) {
            throw new APIErrorException('notFound', 'Category not found');
        }

        return Category::updateWithModel($category)->response();
    }

    /**
     * @param int           $id
     * @param UpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Api\Admin\APIErrorException
     */
    public function update($id, UpdateRequest $request)
    {
        $category = $this->categoryRepository->find($id);
        if (empty($category)) {
            throw new APIErrorException('notFound', 'Category not found');
        }

        $input = $request->only([
        'name',
        ]);

        $category = $this->categoryRepository->update($category, $input);

        return Category::updateWithModel($category)->response();
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Api\Admin\APIErrorException
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->find($id);
        if (empty($category)) {
            throw new APIErrorException('notFound', 'Category not found');
        }

        $this->categoryRepository->delete($category);

        return Status::ok()->response();
    }
}

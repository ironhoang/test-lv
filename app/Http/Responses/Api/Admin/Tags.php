<?php
namespace App\Http\Responses\Api\Admin;

class Tags extends ListBase
{
    protected static $itemsResponseModel = Tag::class;
}

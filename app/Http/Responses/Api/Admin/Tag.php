<?php
namespace App\Http\Responses\Api\Admin;

class Tag extends Response
{
    protected $columns = [
        'id'        => 0,
        'name'      => '',
        'slug'      => null,
        'createdAt' => null,
        'updatedAt' => null,
    ];

    /**
     * @param \App\Models\Tag $model
     *
     * @return static
     */
    public static function updateWithModel($model)
    {
        $response = new static([], 400);
        if (!empty($model)) {
            $modelArray = [
                'id'        => $model->id,
                'name'      => $model->name,
                'slug'      => $model->slug,
                'createdAt' => $model->created_at ? $model->created_at->timestamp : null,
                'updatedAt' => $model->updated_at ? $model->updated_at->timestamp : null,
            ];
            $response   = new static($modelArray, 200);
        }

        return $response;
    }
}

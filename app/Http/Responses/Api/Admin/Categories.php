<?php
namespace App\Http\Responses\Api\Admin;

class Categories extends ListBase
{
    protected static $itemsResponseModel = Category::class;
}

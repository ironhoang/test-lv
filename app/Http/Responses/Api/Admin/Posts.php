<?php
namespace App\Http\Responses\Api\Admin;

class Posts extends ListBase
{
    protected static $itemsResponseModel = Post::class;
}

<?php
namespace App\Presenters;

use LaravelRocket\Foundation\Presenters\BasePresenter;

/**
 * @property  \App\Models\Category $entity
 * @property  int $id
 * @property  string $name
 * @property  \Carbon\Carbon $created_at
 * @property  \Carbon\Carbon $updated_at
 */
class CategoryPresenter extends BasePresenter
{
    protected $multilingualFields = [
    ];

    protected $imageFields = [
    ];

    public function toString()
    {
        return $this->entity->present()->name;
    }
}

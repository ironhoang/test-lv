<?php
namespace App\Presenters;

use LaravelRocket\Foundation\Presenters\BasePresenter;

/**
 * @property  \App\Models\Post $entity
 * @property  int $id
 * @property  string $name
 * @property  string $content
 * @property  \Carbon\Carbon $created_at
 * @property  \Carbon\Carbon $updated_at
 */
class PostPresenter extends BasePresenter
{
    protected $multilingualFields = [
    ];

    protected $imageFields = [
    ];

    public function toString()
    {
        return $this->entity->present()->name;
    }
}

<?php
namespace App\Models;

use LaravelRocket\Foundation\Models\Base;

/**
 * App\Models\AdminUserNotification.
 *
 * @method \App\Presenters\AdminUserNotificationPresenter present()
 *
 * @property int $id
 * @property int $admin_user_id
 * @property string $type
 * @property string|null $data
 * @property int $notified_at
 * @property int $read_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\AdminUser $adminUser
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereAdminUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereNotifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminUserNotification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdminUserNotification extends Base
{
    const TYPE_ALERT       = 'alert';
    const TYPE_INFORMATION = 'information';
    const TYPE_WARNING     = 'warning';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin_user_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_user_id',
        'type',
        'data',
        'notified_at',
        'read_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [
    ];

    protected $casts     = [
    ];

    protected $presenter = \App\Presenters\AdminUserNotificationPresenter::class;

    // Relations
    public function adminUser()
    {
        return $this->belongsTo(\App\Models\AdminUser::class, 'admin_user_id', 'id');
    }

    // Utility Functions
}

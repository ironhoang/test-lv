<?php
namespace Tests\Controllers\Api\Admin;

use Tests\TestCase;

class PostControllerTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var \App\Http\Controllers\Api\Admin\PostController $controller */
        $controller = \App::make(\App\Http\Controllers\Api\Admin\PostController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser     = factory(\App\Models\AdminUser::class)->create();
        $authUserRole = factory(\App\Models\AdminUserRole::class)->create([
            'admin_user_id' => $authUser->id,
            'role'          => \App\Models\AdminUserRole::ROLE_SUPER_USER,
        ]);
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Api\Admin\PostController@index');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $post = factory(\App\Models\Post::class)->make();
        $this->action('POST', 'Api\Admin\PostController@store', $post->toArray());
        $this->assertResponseStatus(201);
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $post = factory(\App\Models\Post::class)->create();

        $testData = str_random(10);
        $id       = $post->id;

        $post->name = $testData;

        $this->action('PUT', 'Api\Admin\PostController@update', [$id], $post->toArray());
        $this->assertResponseStatus(200);

        $newPost = \App\Models\Post::find($id);
        $this->assertEquals($testData, $newPost->name);
    }

    public function testDeleteModel()
    {
        $post = factory(\App\Models\Post::class)->create();

        $id = $post->id;

        $this->action('DELETE', 'Api\Admin\PostController@destroy', [$id]);
        $this->assertResponseStatus(200);

        $checkPost = \App\Models\Post::find($id);
        $this->assertNull($checkPost);
    }
}

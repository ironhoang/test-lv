<?php
namespace Tests\Controllers\Api\Admin;

use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var \App\Http\Controllers\Api\Admin\CategoryController $controller */
        $controller = \App::make(\App\Http\Controllers\Api\Admin\CategoryController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser     = factory(\App\Models\AdminUser::class)->create();
        $authUserRole = factory(\App\Models\AdminUserRole::class)->create([
            'admin_user_id' => $authUser->id,
            'role'          => \App\Models\AdminUserRole::ROLE_SUPER_USER,
        ]);
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Api\Admin\CategoryController@index');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $category = factory(\App\Models\Category::class)->make();
        $this->action('POST', 'Api\Admin\CategoryController@store', $category->toArray());
        $this->assertResponseStatus(201);
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $category = factory(\App\Models\Category::class)->create();

        $testData = str_random(10);
        $id       = $category->id;

        $category->name = $testData;

        $this->action('PUT', 'Api\Admin\CategoryController@update', [$id], $category->toArray());
        $this->assertResponseStatus(200);

        $newCategory = \App\Models\Category::find($id);
        $this->assertEquals($testData, $newCategory->name);
    }

    public function testDeleteModel()
    {
        $category = factory(\App\Models\Category::class)->create();

        $id = $category->id;

        $this->action('DELETE', 'Api\Admin\CategoryController@destroy', [$id]);
        $this->assertResponseStatus(200);

        $checkCategory = \App\Models\Category::find($id);
        $this->assertNull($checkCategory);
    }
}

<?php
namespace Tests\Controllers\Api\Admin;

use Tests\TestCase;

class TagControllerTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var \App\Http\Controllers\Api\Admin\TagController $controller */
        $controller = \App::make(\App\Http\Controllers\Api\Admin\TagController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser     = factory(\App\Models\AdminUser::class)->create();
        $authUserRole = factory(\App\Models\AdminUserRole::class)->create([
            'admin_user_id' => $authUser->id,
            'role'          => \App\Models\AdminUserRole::ROLE_SUPER_USER,
        ]);
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Api\Admin\TagController@index');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $tag = factory(\App\Models\Tag::class)->make();
        $this->action('POST', 'Api\Admin\TagController@store', $tag->toArray());
        $this->assertResponseStatus(201);
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $tag = factory(\App\Models\Tag::class)->create();

        $testData = str_random(10);
        $id       = $tag->id;

        $tag->name = $testData;

        $this->action('PUT', 'Api\Admin\TagController@update', [$id], $tag->toArray());
        $this->assertResponseStatus(200);

        $newTag = \App\Models\Tag::find($id);
        $this->assertEquals($testData, $newTag->name);
    }

    public function testDeleteModel()
    {
        $tag = factory(\App\Models\Tag::class)->create();

        $id = $tag->id;

        $this->action('DELETE', 'Api\Admin\TagController@destroy', [$id]);
        $this->assertResponseStatus(200);

        $checkTag = \App\Models\Tag::find($id);
        $this->assertNull($checkTag);
    }
}

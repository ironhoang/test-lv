<?php
namespace Tests\Repositories;

use App\Models\Post;
use Tests\TestCase;

class PostRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $models  = factory(Post::class, 3)->create();
        $postIds = $models->pluck('id')->toArray();

        /** @var \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $modelsCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(Post::class, $modelsCheck[0]);

        $modelsCheck = $repository->getByIds($postIds);
        $this->assertEquals(3, count($modelsCheck));
    }

    public function testFind()
    {
        $models  = factory(Post::class, 3)->create();
        $postIds = $models->pluck('id')->toArray();

        /** @var \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postCheck = $repository->find($postIds[0]);
        $this->assertEquals($postIds[0], $postCheck->id);
    }

    public function testCreate()
    {
        $postData = factory(Post::class)->make();

        /** @var \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postCheck = $repository->create($postData->toFillableArray());
        $this->assertNotNull($postCheck);
    }

    public function testUpdate()
    {
        $postData = factory(Post::class)->create();

        /** @var \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postData = factory(Post::class)->make();

        $postCheck = $repository->update($postData, $postData->toFillableArray());
        $this->assertNotNull($postCheck);
    }

    public function testDelete()
    {
        $postData = factory(Post::class)->create();

        /** @var \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($postData);

        $postCheck = $repository->find($postData->id);
        $this->assertNull($postCheck);
    }
}
